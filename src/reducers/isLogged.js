const loggedReducer = (state = false, action) => {
  switch (action.type) {
    case "SIGN_IN":
      // !state is opposite of state
      return !state;
    default:
      return state;
  }
};

export default loggedReducer;
